# A Role-playing game to study JS

## STUDYING at www.scrimba.com

- Classes
- Constructors
- Logic

## CHALENGES
# CHALENGE 1 
Challenge: 
1. Take the hard-coded HTML for the Wizard card, bring it 
   into index.js and then inject it back into its div with 
   JavaScript.
2. Do the same for Orc card. 

# CHALENGE 2
1. Strip out the hero and monster data (element id, name, avatar, 
health and dice score) and store them in variables

2. Write a renderCharacter() function that accepts the 5 new variables 
as paramaters and renders out a character with this data

3. Call renderCharacter() twice. Once with the hero variables and 
once with the monster variables to that both are rendered

# CHALENGE 3

CHALLENGE
1. declare a let called diceHtml and initialize it with an empty 
string. 
2. Use a for loop to update diceHtml so that it contains the 
HTML for our dice. The number of dice needed is specificed in 
the diceCount property of the objects.
3. Each dice should have the following HTML: <div class="dice">6</div>
4. For now, each dice will display 6
5. Swap out the diceRoll variable for diceHtml in the template

# CHALENGE 4

Challenge
1. Create a function called getDiceRollArray that uses a 
   for loop to return an array of random numbers between 1-6. 
2  The function should have diceCount as a parameter and the 
   array it returns should be diceCount length.
3  For testing purposes, call the function with a diceCount of 
   3 and log out the result. 

# CHALENGE 5

Challenge 
1. Create a function called getDiceHtml. 
2. getDiceHtml should map over the array of dice rolls 
   returned from getDiceRollArray to generate the html 
   we need to render our dice with random values. This is 
   the HTML: `<div class="dice">DICE VALUE HERE</div>`
3. Think about the parameters and arguments!
4. Down in renderCharacter(), set diceHtml equals to our 
   new getDiceHtml function. Remember to give it the argument
   it needs. 
5. Delete any code we no longer need.
**hint.md for help**

## CHALENGE 6

1. Create a new constructor function called Character which
  takes our data as a paramenter.
2. Set up "this" for each of the 5 properties in our objects
  (eg: this.health = data.health).
