const getDiceRollArray = (diceCount) => {
  return new Array(diceCount)
    .fill(0)
    .map(() => Math.floor(Math.random() * 6) + 1);
};

const getDicePlaceholderHtml = (diceCount) => {
  return new Array(diceCount)
    .fill(0)
    .map(() => `<div class="placeholder-dice"></div>`)
    .join("");
}

const getPercentage = (remaingHealth, maximumHealth) =>
  (100 * remaingHealth) / maximumHealth;

export { getDiceRollArray, getDicePlaceholderHtml, getPercentage };
